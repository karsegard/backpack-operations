<?php

namespace KDA\Backpack\Operations;

use Illuminate\Support\Facades\Route;

trait ExternalLinkOperation
{
    function setExternalLinkAccessor($callback)
    {
        $this->crud->macro('externalLinkAccessor', $callback);
    }
    function setShouldDisplayExternalLink($callback)
    {
        $this->crud->macro('shouldDisplayExternalLink', $callback);
    }

    /**
     * Define which routes are needed for this operation.
     *
     * @param string $segment    Name of the current entity (singular). Used as first URL segment.
     * @param string $routeName  Prefix of the route name.
     * @param string $controller Name of the current CrudController.
     */
    protected function setupExternalLinkRoutes($segment, $routeName, $controller)
    {
        Route::get($segment . '/{id}/externallink/{externallink}', [
            'as'        => $routeName . '.externallink',
            'uses'      => $controller . '@externallink',
            'operation' => 'externallink',
        ]);
    }

    /**
     * Add the default settings, buttons, etc that this operation needs.
     */
    protected function setupExternalLinkDefaults()
    {
        $this->crud->allowAccess('externallink');

        $this->crud->operation('externallink', function () {
            $this->crud->loadDefaultOperationSettingsFromConfig();
        });

        $this->crud->operation('list', function () {
            $this->crud->addButton('line', 'externallink', 'view', 'kda-backpack::buttons.externallink');
        });
    }



    /**
     * Show the view for performing the operation.
     *
     * @return Response
     */
    public function externallink($id, $externallink)
    {
        $this->crud->hasAccessOrFail('externallink');


        return back();
    }
}
