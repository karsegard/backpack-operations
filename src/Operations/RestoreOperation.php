<?php

namespace KDA\Backpack\Operations;

use Illuminate\Support\Facades\Route;

trait RestoreOperation
{


    /**
     * Define which routes are needed for this operation.
     *
     * @param string $segment    Name of the current entity (singular). Used as first URL segment.
     * @param string $routeName  Prefix of the route name.
     * @param string $controller Name of the current CrudController.
     */
    protected function setupRestoreRoutes($segment, $routeName, $controller)
    {
        Route::get($segment . '/{id}/restore', [
            'as'        => $routeName . '.restore',
            'uses'      => $controller . '@restore',
            'operation' => 'restore',
        ]);
    }

    /**
     * Add the default settings, buttons, etc that this operation needs.
     */
    protected function setupRestoreDefaults()
    {
        $this->crud->allowAccess('restore');

        $this->crud->operation('restore', function () {
            $this->crud->loadDefaultOperationSettingsFromConfig();
        });

        $this->crud->operation('list', function () {
            $this->crud->addButton('line', 'restore', 'view', 'kda-backpack::buttons.restore');
        });
    }



    /**
     * Show the view for performing the operation.
     *
     * @return Response
     */
    public function restore($id)
    {
        $this->crud->hasAccessOrFail('restore');
        try {


            $model = $this->crud->getModel();
            $entry = $model::withTrashed()->find($id);
            
            $entry->restore();
            \Alert::success(__("Item restored"))->flash();
        } catch (\Error $e) {
            \Alert::error($e->getMessage())->flash();
        }

        return back();
    }
}
