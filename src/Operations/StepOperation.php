<?php

namespace KDA\Backpack\Operations;

use Illuminate\Support\Facades\Route;

trait StepOperation
{


    function setSteps($steps)
    {
        $this->crud->macro('getSteps', function () use ($steps) {

            return $steps;
        });
    }


    function setStepAccessor($callback)
    {
        $this->crud->macro('stepAccessor', $callback);
    }
    function setStepMutator($callback)
    {
        $this->crud->macro('stepMutator', $callback);
    }
    function setStepShouldDisplay($callback)
    {
        $this->crud->macro('stepShouldDisplay', $callback);
    }
    /**
     * Define which routes are needed for this operation.
     *
     * @param string $segment    Name of the current entity (singular). Used as first URL segment.
     * @param string $routeName  Prefix of the route name.
     * @param string $controller Name of the current CrudController.
     */
    protected function setupStepRoutes($segment, $routeName, $controller)
    {
        Route::get($segment . '/{id}/step/{step}', [
            'as'        => $routeName . '.step',
            'uses'      => $controller . '@step',
            'operation' => 'step',
        ]);
    }

    /**
     * Add the default settings, buttons, etc that this operation needs.
     */
    protected function setupStepDefaults()
    {
        $this->crud->allowAccess('step');
        $this->crud->setOperationSetting('current_step_attribute', 'status', 'step');

        $this->crud->operation('step', function () {
            $this->crud->loadDefaultOperationSettingsFromConfig();
        });

        $this->crud->operation('list', function () {
            $this->crud->addButton('line', 'step', 'view', 'kda-backpack::buttons.step');
        });
    }



    /**
     * Show the view for performing the operation.
     *
     * @return Response
     */
    public function step($id, $step)
    {
        $this->crud->hasAccessOrFail('step');
        try {



            $action = $this->crud->get('step.current_step_attribute');
            $entry = $this->crud->getEntry($id);

            if ($this->crud->hasMacro('stepMutator')) {
                $this->crud->stepMutator($entry, $step, $action);
            } else {
                $entry->$action = $step;
                $entry->save();
            }
            $_step = $this->crud->getSteps()[$step];
            $message= $_step['message_done'] ?? 'Updated';
            \Alert::success($message)->flash();
        } catch (\Error $e) {
            \Alert::error($e->getMessage())->flash();
        }

        return back();
    }
}
