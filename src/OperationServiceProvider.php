<?php

namespace KDA\Backpack;

use KDA\Laravel\PackageServiceProvider;

class OperationServiceProvider extends PackageServiceProvider
{
   use \KDA\Laravel\Traits\HasViews;



   protected function packageBaseDir () {
       return dirname(__DIR__,1);
   }


   public function boot(){
    parent::boot();
    $this->loadViewsFrom( $this->path($this->viewsDir,'/vendor/backpack/crud'), 'kda-backpack');
   }

}