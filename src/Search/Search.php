<?php

namespace KDA\Backpack\Search;


trait Search
{

    public function parseQueryTerm($searchTerm): array
    {
        $re = '/(?<!")\b\w+\b|(?<=")\b[^"]+/m';

        preg_match_all($re, trim($searchTerm), $matches, PREG_SET_ORDER, 0);

        $terms = [];

        foreach ($matches as $m) {
            if ($m[0] !== "") {
                $terms[] = $m[0];
            }
        }
        return $terms;
    }

    public function searchInRelation($relation = "book")
    {
        return function ($query, $column, $searchTerm) use ($relation) {

            $query->orWhereHas($relation, $this->searchMultiTermsCriteria($searchTerm, $column));
        };
    }

    public function searchColumn()
    {

        return function ($query, $column, $searchTerm) {

            $query->where($this->searchMultiTermsCriteria($searchTerm, $column));
        };
    }



    public function searchMultiTermsCriteria($searchTerm, $column)
    {
        $attributes = $column;
        if (!is_string($column)) {
            $attributes = $column['searchAttributes'] ?? [$column['key']];
        }
        $terms = $this->parseQueryTerm($searchTerm);
      

        return function ($q) use ($terms, $attributes) {
            foreach ($terms as $term) {
                $q->where(function ($q) use ($attributes, $term) {
                    foreach ($attributes as $column) {
                        $q->orWhere($column, 'like', '%' . $term . '%');
                    }
                });
            }
        };
    }
}
