@if ($crud->hasAccess('externallink'))
    @if ($crud->shouldDisplayExternalLink($entry))
        @php
            
            $link = $crud->externalLinkAccessor($entry);
        @endphp
        <a href="{{$link['href'] ?? ''}}" target="{{$link['target']??''}}" onclick="" class="btn btn-sm btn-link" data-style="zoom-in"><span
                class="ladda-label"><i class="la "></i>{{$link['label']??'External link'}}</span></a>

        <script>

        </script>
    @endif
@endif
