@if ($crud->hasAccess('restore'))
    @if ($entry->trashed())
        <a href="{{ url($crud->route . '/' . $entry->getKey() . '/restore') }}" target="{{ $link['target'] ?? '' }}"
            onclick="" class="btn btn-sm btn-link" data-style="zoom-in"><span class="ladda-label"><i
                    class="la "></i>{{ __('Restore') }}</span></a>
    @endif
@endif
