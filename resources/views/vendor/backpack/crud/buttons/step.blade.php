@if ($crud->hasAccess('step'))
    @php
        $steps = $crud->getSteps();
        $key = $crud->get('step.current_step_attribute');
        
        $current = null;
        if (!$crud->hasMacro('stepAccessor')) {
            $current = $entry->$key;
        } else {
            $current = $crud->stepAccessor($entry, $key);
        }
        
        $stepShouldDisplay = function () {
            return true;
        };
        
        if ($crud->hasMacro('stepShouldDisplay')) {
            $stepShouldDisplay = function($entry,$step,$key) use ($crud){
                return $crud->stepShouldDisplay($entry,$step,$key);
            };
        }
        
        $step = $steps[$current];
    @endphp
    @foreach ($step['next_steps'] as $next_step)
        @php
            $step = $steps[$next_step];
        @endphp
        @if ($stepShouldDisplay($entry, $next_step,$current))
            <a href="javascript:void(0)" onclick="performStep(this)"
                data-confirm="{{ isset($step['message_confirm']) ? $step['message_confirm'] : '' }}"
                data-route="{{ url($crud->route . '/' . $entry->getKey() . '/step/' . $next_step) }}" href=""
                class="btn btn-sm btn-link" data-style="zoom-in"><span class="ladda-label"><i
                        class="la {{ isset($step['icon']) ? $step['icon'] : '' }}"></i>{{ $step['action_label'] }}</span></a>
        @endif
    @endforeach
    <script>
        function performStep(button) {
            var route = $(button).attr('data-route');
            var _confirm = $(button).attr('data-confirm') !== "";
            if (_confirm) {

                swal({
                    title: "{!! trans('backpack::base.warning') !!}",
                    text: $(button).attr('data-confirm'),
                    // icon: "warning",
                    buttons: ["{!! trans('backpack::crud.cancel') !!}", "{!! trans('backpack::crud.yes') !!}"],
                    dangerMode: true,
                }).then(result => {
                    if (result == 1) {
                        window.location.href = route;
                    }

                })

            } else {
                window.location.href = route;
            }
        }
    </script>
@endif
